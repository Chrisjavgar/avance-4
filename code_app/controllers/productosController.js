var session = require('express-session');

const productController = {};

productController.save = (req, res) =>{
    const clientes = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO `Cliente` set ?', [clientes], (err, clients) =>{
            res.redirect('/login.ejs')
        })
    })
};


productController.login = (req, res) =>{
    const usuario = req.body;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM `Cliente` WHERE usuario = ? AND contraseña = ?', [usuario], (err, clients) =>{
            res.redirect('/nosotros.ejs')
        })
    })
};

module.exports = productController; 