const express = require('express');
const rutas = express.Router();

rutas.get('/', (req, res) =>{
    res.render('index.ejs', {title: 'Inicio'});
});

rutas.get('/login.ejs', (req, res) =>{
    res.render('login.ejs', {title: 'Iniciar Sesion'});
});

rutas.get('/productCar.ejs', (req, res) =>{
    res.render('productCar.ejs', {title: 'Carrito'});
});

rutas.get('/nosotros.ejs', (req, res) =>{
    res.render('nosotros.ejs', {title: 'Quienes Somos'});
});

rutas.get('/prueba.ejs', (req, res) =>{
    res.render('prueba.ejs', {title: 'Quienes Somos'});
});

module.exports = rutas;