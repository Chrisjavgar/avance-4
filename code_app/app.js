const express = require('express');
const path = require('path')
const mysql = require('mysql')
const conn = require('express-myconnection')

const app = express();

//CONFIG DE PUERTO
app.set('port', process.env.PORT || 3030);

//CONFIG MOTOR DE PLANTILLAS - VISTAS
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');


//CONFIG DB
app.use(conn(mysql,{
    host: 'localhost',
    user: 'root',
    password: '',
    port: 3306,
    database: 'db_security_mastered'
}, 'single'));


//MIDDLEWARES
app.use(express.urlencoded({extended: true}))

//RUTAS DE LA APLICACION
app.use(require('./routes/rutas'));
app.use(require('./routes/rutasdb'))

//ARCHIVOS ESTATICOS
app.use(express.static(path.join(__dirname, 'public')))

//SERVIDOR
app.listen(app.get('port'), () =>{
    console.log(`Soy el puerto que funciona, soy el puerto: ${app.get('port')}`);
})